# courier

courier is a service for slack that is used to post your Strava club activities to a designated slack channel.

[![Codacy Badge](https://api.codacy.com/project/badge/grade/4657515d9ab24668907b4d43f248978f)](https://www.codacy.com)


## Installation

1. Clone this repo
2. Run `npm install`


## Environment variables

To run locally you'll need...

- AUTH_USERNAME=...
- AUTH_PASSWORD=...
- BASE_URL=http://localhost:8127
- GOOGLE_MAPS_API_KEY=...
- PORT=8127
- MONGO_URL=mongodb://localhost/courier
- POLL_FREQUENCY=hour
- PRODUCTS_FREQUENCY=biweekly
- LEADERBOARD_FREQUENCY=week
- REQUEST_PERIOD=week
- PRODUCTS_URL=http://localhost:3000
- PRODUCTS_QUANTITY=3
- STRAVA_APP_ID=...
- STRAVA_APP_SECRET=...
- SLACK_CLIENT_ID=...
- SLACK_CLIENT_SECRET=...

## Running locally

- Install ngrok (https://ngrok.com/download)
- Unzip and copy to `/user/local/bin` so that it's available from command line
- Add the auth token `$ ngrok authtoken XXX.XXX.XXX.XXX`
- Run ngrok `$ ngrok http -subdomain=getcourier -region=eu 8127`
- Visit https://getcourier.eu.ngrok.io/ *

* https://getcourier.eu.ngrok.io/ is set up as a callback url here https://api.slack.com/apps/A0Z7F46DD/oauth

## Deploying

- `git remote set-url deploy dokku@XXX.XXX.XXX.XXX:courier`
- `git push deploy master`

## Remote server
- dokku on DigitalOcean

### Security

- Using LetsEncrypt for SSL certificate (https://letsencrypt.org/)
- And dokku plugin for LetsEncrypt (https://github.com/dokku/dokku-letsencrypt)
- We've set up a cron job to auto-renew the SSL cert (https://blog.semicolonsoftware.de/running-dokku-letsencrypt-auto-renewal-as-a-cronjob/)
- We've installed Helmet for ExpressJS security (https://blog.risingstack.com/node-js-security-checklist/)

### Backups

- We've set up AWS S3 bucket (after setting up IAM user)
- We've installed and configured S3CMD on Dokku (http://s3tools.org/s3cmd — getcourier)
- We've set up a cron job to send daily dumps to S3

### Installing s3cmd
- Need s3cmd for backing up db to AWS S3
- `apt-get s3cmd`
- `s3cmd s3cmd --configure`

### Some notes on cron:
  - Have to escape `%` otherwise it gets converted to a new line character and everything will fail
  - Email doesn't seem to work (errors don't get sent to email address)
  - Need a new line at the end of the file
  - Run as root
  - Logs to /var/log/syslog
  - Have to specify path to S3 config file (/root/.s3cfg) in command

cron file looks like this:

```
SHELL=/bin/bash
HOME=/
PATH=/usr/local/bin:/usr/bin:/bin
MAILTO=””
# m h  dom mon dow   command

# auto-renew the SSL cert
0 1 * * * dokku letsencrypt:auto-renew &>> /var/log/dokku/letsencrypt.log

# backup db to S3
0 1 * * * dokku mongo:export courier-app | gzip -9 > tmp.gz && s3cmd -c /root/.s3cfg put tmp.gz s3://courier-db/backup-$(date +"\%Y-\%m-\%d").gz && rm tmp.gz >> /var/log/dokku/courier.log 2>&1
```


### Importing from backup
*Note: this is wrong — need to get from S3*

1. Download latest backup from S3
2. Rename to courier-dump.gz
2. `scp courier-dump.gz root@XXX.XXX.XXX.XXX:/home/dokku/courier`
1. `ssh root@XXX.XXX.XXX.XXX`
1. `cd /home/dokku/courier`
1. `gunzip courier-dump.gz`
3. `dokku mongo:import courier < courier-dump`

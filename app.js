// External dependencies
var express = require( 'express' ),
    helmet = require( 'helmet' ),
    exphbs  = require( 'express-handlebars' ),
    bodyParser = require( 'body-parser' ),
    compression = require( 'compression' ),
    favicon = require( 'serve-favicon' );

// Dependencies
var router = require( './app/core/router' ),
    db = require( './app/lib/db' ),
    jobs = require( './app/core/jobs' ),
    scheduler = require( './app/core/scheduler' ),
    logger = require( './app/lib/logger' ),
    environment = require( './app/lib/environment' );

// Local variables
var app;

/**
 * Init
 */
function init() {
  // Set up app
  app = configureExpress( express() );

  // Start the server
  app.listen( environment.port, function () {
    logger.info( "Server started\n", JSON.stringify( environment, null, 2 ) );
  } );

  // Connect to db after starting express
  db.init();

  if ( environment.name !== 'test' ) {
    // And schedule all jobs
    scheduler.scheduleJobs();

    // Kick off the main activity
    jobs.runActivitiesJobs();
  }
}

function configureExpress( app ) {
  if ( environment.name === 'production' ) {
    app.use( compression() );
  }
  app.use( favicon( __dirname + '/app/public/ico/favicon.ico' ) );
  app.set( 'views', __dirname + '/app/views/' );
  app.engine( 'hbs', exphbs( {
    extname: '.hbs',
    layoutsDir: app.settings.views + 'layouts/',
    defaultLayout: 'main'
  } ) );
  app.set( 'view engine', 'hbs' );
  app.use( helmet() );
  app.use( bodyParser.urlencoded( { extended: true } ) );
  app.use( express.static( __dirname + '/app/public/' ) );
  app.use( '/', router );

  return app;
}

// Kick it off...
init();

module.exports = app;

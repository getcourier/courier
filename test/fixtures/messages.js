// Dependencies
var products = require( "./products" ).products;

module.exports = {

      "activityMessages": [ { 
        "username": "Joe on Strava (via #courier)",
        "icon_url": "https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg",
        "attachments":
         [ { "fallback": "Joe ran 8.6km in 37:34",
             "fields": [],
             "color": "#fc4c02",
             "title": "I ran 8.6km in 37:34",
             "text": "<https://www.strava.com/activities/392538416|Wet and windy morning Run>" 
           },
           { "fallback": "Average pace was 4:21 mins/km. Total elevation was 25.3m.",
              "fields": [
                  {
                    "short": true,
                    "title": "Average pace",
                    "value": "4:21 mins/km"
                  },
                  {
                    "short": true,
                    "title": "Elevation",
                    "value": "25.3m"
                  }
              ],
             "color": "#fc4c02",
             "image_url": "http://maps.googleapis.com/maps/api/staticmap?sensor=false&maptype=terrain&size=1600x400&path=weight:3|color:red|enc:{lguHbmu@~BvGiG`JoK~E_YjX{A|a@lCGh@gJnKsKnY}MnHyUfEiFkG}PoRgF}KuJ{K`GjHyGnRT`BsQaAmBk\\yJ^ad@aL_a@k@uS`e@fBsClr@zIr@zAlBAvC&key=undefined" 
           } 
          ]    
        }, { 
      "username": "Matt on Strava (via #courier)",
      "icon_url": "https://dgalywyr863hv.cloudfront.net/pictures/athletes/3915456/1276644/1/large.jpg",
      "attachments": [ { 
        "fallback": "Matt ran 3.3km in 17:50",
        "fields": [],
        "color": "#fc4c02",
        "title": "I ran 3.3km in 17:50",
        "text": "<https://www.strava.com/activities/392180324|Windy Afternoon Run>" 
      }, { 
        "fallback": "Average pace was 5:20 mins/km. Total elevation was 10.3m.",
        "fields": [
                {
                  "short": true,
                  "title": "Average pace",
                  "value": "5:20 mins/km"
                },
                {
                  "short": true,
                  "title": "Elevation",
                  "value": "10.3m"
                }
            ],
        "color": "#fc4c02",
        "image_url": null 
      } ] 
    } ], 

    "leaderboardMessages": [ { 
      "text": "See the whole leaderboard on <https://www.strava.com/clubs/131144|Strava>",
      "username": "Weekly leaderboard (via #courier)",
      "icon_emoji": ":trophy:",
      attachments:
       [ { "color": "#fc4c02",
           "thumb_url": "https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg",
           "title": "Longest distance",
           "text": "*Joe Birch: 8.6km*:sports_medal: \nRobert Douglas: 5.8km\n",
           "fallback": "",
           "mrkdwn_in": ["text"] },
         { "color": "#fc4c02",
           "thumb_url": "https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg",
           "title": "Total distance",
           "text": "*Joe Birch: 13.6km*:sports_medal: \nRobert Douglas: 5.8km\n",
           "fallback": "",
           "mrkdwn_in": ["text"] },
         { "color": "#fc4c02",
           "thumb_url": "https://dgalywyr863hv.cloudfront.net/pictures/athletes/2227472/827183/1/large.jpg",
           "title": "Total climb",
           "text": "*Robert Douglas: 114.0m*:sports_medal: \nJoe Birch: 40.6m\n",
           "fallback": "",
           "mrkdwn_in": ["text"] 
         } ] 
      },
      { 
        "text": "See the whole leaderboard on <https://www.strava.com/clubs/131144|Strava>",
        "username": "Weekly leaderboard (via #courier)",
        "icon_emoji": ":trophy:",
        attachments:
         [ { "color": "#fc4c02",
             "thumb_url": "",
             "title": "",
             "text": "No activity this week? Shame on you all…",
             "fallback": "",
             "mrkdwn_in": ["text"] 
           } ] 
      } ],

    "productsMessages": [ { 
      "text": "Help us maintain #courier by buying a little something below…",
      "username": "#courier",
      "icon_emoji": ":trophy:",
      "attachments": [ 
        {
          "color": "#fc4c02",
          "thumb_url": products[ 0 ].image,
          "title": "<" + products[ 0 ].link + "|" + products[ 0 ].title + ">",
          "text": products[ 0 ].description,
          "fallback": products[ 0 ].description
        }, {
          "color": "#fc4c02",
          "thumb_url": products[ 1 ].image,
          "title": "<" + products[ 1 ].link + "|" + products[ 1 ].title + ">",
          "text": products[ 1 ].description,
          "fallback": products[ 1 ].description
        }, {
          "color": "#fc4c02",
          "thumb_url": products[ 2 ].image,
          "title": "<" + products[ 2 ].link + "|" + products[ 2 ].title + ">",
          "text": products[ 2 ].description,
          "fallback": products[ 2 ].description
         }
        ]
      } ]

};
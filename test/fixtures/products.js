module.exports = {
  "products": [
    {
      "_id": "5713a47da09c65707af2a20c",
      "title": "Title 1",
      "description": "Some men's shoes",
      "image": "image1.png",
      "link": "http://link.to/product1",
      "activity": "Run",
      "category": "Shoes",
      "gender": "Male",
      "createdAt": "2016-04-17T14:58:05.586Z",
      "active": true
    },
    {
      "_id": "5713a47da09c65707af2a20d",
      "title": "Title 2",
      "description": "Some women's shoes",
      "image": "image2.png",
      "link": "http://link.to/product2",
      "activity": "Run",
      "category": "Shoes",
      "gender": "Female",
      "createdAt": "2016-04-17T14:58:05.741Z",
      "active": true
    },
    {
      "_id": "KS3LJ8sJTHDwN3NEj",
      "title": "Under Armour Men's HeatGear Long Sleeve Compression Top",
      "description": "Designed around UA's HeatGear fabric technology and stripped down to deliver a supremely lightweight construction, the garment thrives when you step up the tempo. HeatGear technology operates around a moisture management system, rapidly drawing perspiration away from the skin into the mid/outer layer to be evaporated.",
      "image": "https://images-na.ssl-images-amazon.com/images/I/81q4AUPaOYL._SL1500_.jpg",
      "link": "http://amzn.to/1MMJgYR",
      "activity": "Run",
      "category": "Apparel",
      "gender": "Any",
      "active": true,
      "createdAt": "2016-04-22T12:53:59.614Z"
    },
    {
      "_id": "AiX8QLiwwusKoCDB4",
      "title": "Asics Gt-2000 4, Men's Training Running Shoes",
      "description": "Meet the latest update of the core runner's bread and butter, the Men's ASICS GT-2000TM 4. Step into the perfect ride, with new, angled Convergence GEL for increased cushioning; and the new Heel Clutching System for a more supportive fit.",
      "image": "https://images-na.ssl-images-amazon.com/images/I/91LcWpCeWPL._UX575_.jpg",
      "link": "http://amzn.to/1QrZG3w",
      "activity": "Run",
      "category": "Shoes",
      "gender": "Male",
      "active": true,
      "createdAt": "2016-04-22T13:02:48.170Z"
    }
  ]
}
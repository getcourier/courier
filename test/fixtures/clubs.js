module.exports = {
  clubs: [
    // 0: Running
    {
      "id": 131144,
      "resource_state": 3,
      "name": "ribot_running",
      "profile_medium": "http://pics.com/clubs/1/medium.jpg",
      "profile": "http://pics.com/clubs/1/large.jpg",
      "description": "The official ribot running club",
      "club_type": "company",
      "sport_type": "running",
      "city": "Brighton",
      "state": "Brighton",
      "country": "United Kingdom",
      "private": false,
      "member_count": 11
    },
    // 1: Cycling
    {
      "id": 131145,
      "resource_state": 3,
      "name": "ribot_cycling",
      "profile_medium": "http://pics.com/clubs/1/medium.jpg",
      "profile": "http://pics.com/clubs/1/large.jpg",
      "description": "The official ribot running club",
      "club_type": "company",
      "sport_type": "cycling",
      "city": "Brighton",
      "state": "Brighton",
      "country": "United Kingdom",
      "private": false,
      "member_count": 11
    },
    // 2: Triathlon
    {
      "id": 131146,
      "resource_state": 3,
      "name": "ribot_triathlon",
      "profile_medium": "http://pics.com/clubs/1/medium.jpg",
      "profile": "http://pics.com/clubs/1/large.jpg",
      "description": "The official ribot running club",
      "club_type": "company",
      "sport_type": "triathlon",
      "city": "Brighton",
      "state": "Brighton",
      "country": "United Kingdom",
      "private": false,
      "member_count": 11
    },
    // 3: Other
    {
      "id": 131146,
      "resource_state": 3,
      "name": "ribot_triathlon",
      "profile_medium": "http://pics.com/clubs/1/medium.jpg",
      "profile": "http://pics.com/clubs/1/large.jpg",
      "description": "The official ribot running club",
      "club_type": "company",
      "sport_type": "other",
      "city": "Brighton",
      "state": "Brighton",
      "country": "United Kingdom",
      "private": false,
      "member_count": 11
    }
  ]
};
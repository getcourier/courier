module.exports = {

    leaderboards: [ { 
      longestDistance:
       [ { name: 'Joe Birch',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg',
           value: 8621.2 },
         { name: 'Robert Douglas',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/2227472/827183/1/large.jpg',
           value: 5819.5 } ],
      totalDistance:
       [ { name: 'Joe Birch',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg',
           value: 13626.2 },
         { name: 'Robert Douglas',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/2227472/827183/1/large.jpg',
           value: 5819.5 } ],
      totalClimb:
       [ { name: 'Robert Douglas',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/2227472/827183/1/large.jpg',
           value: 114 },
         { name: 'Joe Birch',
           avatar: 'https://dgalywyr863hv.cloudfront.net/pictures/athletes/4001400/2434613/2/large.jpg',
           value: 40.6 } ]
    }]

};
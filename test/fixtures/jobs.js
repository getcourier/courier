var clubs = require( '../fixtures/clubs' ).clubs,
    athletes = require( '../fixtures/athletes' ).athletes;

module.exports = {

  jobs: [
    {
      "_id": "55f960683ae98442433d5b91",
      "strava": {
        "accessToken": "b3ed576458635504bb0ecee340a54001a52949ac",
      },
      "athlete": athletes[ 0 ],
      "club": clubs[ 0 ],
      "slack": {
        "accessToken": "dummy_access_token",
        "team": {
          "name": "anucreative",
          "id": "dummy_id"
        },
        "webhook": {
          "url": "https://hooks.slack.com/services/fake_url",
          "channel": "running"
        }
      },
      active: true
    },
    {
      "_id": "55f9b9603ae98442433d5b92",
      "strava": {
        "accessToken": "b3ed576458635504bb0ecee340a54001a52949ac",
      },
      "athlete": athletes[ 0 ],
      "club": clubs[ 0 ],
      "slack": {
        "accessToken": "dummy_access_token",
        "team": {
          "name": "anucreative",
          "id": "dummy_id"
        },
        "webhook": {
          "url": "https://hooks.slack.com/services/fake_url",
          "channel": "running"
        }
      },
      active: true
    },
    {
      "_id": "55f9b9603ae98442433d5b93",
      "strava": {
        "accessToken": "b3ed576458635504bb0ecee340a54001a52949ac",
      },
      "athlete": athletes[ 0 ]
    }
  ]
};
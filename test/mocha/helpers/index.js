// External dependencies
var Promise = require( 'bluebird' );

// Dependencies
var db = require( '../../../app/lib/db' ),
    jobs = require( '../../fixtures/jobs' ).jobs,
    activities = require( '../../fixtures/activities' ).activities;

var helpers = {
  dropDbs: function () {
    return Promise.all( [
      db.drop( 'activities' ),
      db.drop( 'jobs' )
    ] );
  },

  primeJobsCollection: function () {
    return db.save( 'jobs', jobs );
  },

  primeActivitiesCollection: function () {
    return db.save( 'activities', activities );
  },

  saveActivities: function( activities ) {
    return Promise.map( activities, function( activity ) {
      return db.saveActivity( activity )
        .catch( function( e ) {
          if ( e.code !== 11000 ) {
            throw e;
          }
        } );
    } );
  }
};

module.exports = helpers;
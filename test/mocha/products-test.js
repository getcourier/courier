// External dependencies
var should = require( 'should' ),
  mocha = require( 'mocha' ),
  nock = require( 'nock' );

var describe = mocha.describe;
var it = mocha.it;

// Dependencies
var products = require( '../../app/core/products' ),
  environment = require( '../../app/lib/environment' ),
  jobs = require( '../fixtures/jobs' ).jobs,
  fixtures = require( '../fixtures/products' );

describe( 'Products', function () {
  /**
   * Get club details from API
   */
  describe( 'Get products', function () {
    var job = jobs[ 0 ];
    job.club.activity_types = [ 'Run' ];

    it( 'should return 3 products', function () {
      nock( environment.products.url )
        .get( '/products' )
        .query( true )
        .reply( 200, fixtures );

      return products.getProducts( job ).then( function fulfilled( data ) {
        data.length.should.eql( 3 );
      } );
    } );

    it( 'should handle no products', function () {
      nock( environment.products.url )
        .get( '/products' )
        .query( true )
        .reply( 200, {} );

      return products.getProducts( job ).then( function fulfilled( data ) {
        should.not.exist( data );
      } );
    } );

    it( 'should handle 500', function () {
      nock( environment.products.url )
        .get( '/products' )
        .query( true )
        .reply( 500, {} );

      return products.getProducts( job ).then( function fulfilled( data ) {
        should.not.exist( data );
      } );
    } );
  } );
} );

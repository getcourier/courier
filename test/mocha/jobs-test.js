// Dependencies
var db = require( '../../app/lib/db' ),
  testHelpers = require( './helpers' ),
  mocha = require( 'mocha' );

var describe = mocha.describe;
var it = mocha.it;
var beforeEach = mocha.beforeEach;

describe( 'Jobs', function () {
  /**
   * Get jobs from db
   */
  describe( 'Get jobs', function () {
    // Prime test db
    beforeEach( function () {
      return testHelpers.primeJobsCollection();
    } );

    it( 'should get 3 jobs from db', function () {
      db.getJobs().then( function ( jobs ) {
        jobs.should.have.length( 3 );
      } );
    } );

    it( 'should get 2 active jobs from db', function () {
      db.getActiveJobs().then( function ( jobs ) {
        jobs.should.have.length( 2 );
      } );
    } );
  } );
} );

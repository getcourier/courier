// External dependencies
var should = require( 'should' ),
  mocha = require( 'mocha' ),
  _ = require( 'lodash' ),
  moment = require( 'moment' ),
  nock = require( 'nock' );

var describe = mocha.describe;
var it = mocha.it;

// Dependencies
var strava = require( '../../app/core/strava' ),
  activities = require( '../fixtures/activities' ).activities,
  leaderboard = require( '../../app/core/leaderboard' ),
  testHelpers = require( './helpers' ),
  jobs = require( '../fixtures/jobs' ).jobs,
  clubs = require( '../fixtures/clubs' ).clubs,
  leaderboards = require( '../fixtures/leaderboards' ).leaderboards;

describe( 'Strava', function () {
  /**
   * Get club details from API
   */
  describe( 'Get club', function () {
    it( 'should handle a running club', function () {
      var club = clubs[ 0 ];
      nock( 'https://www.strava.com' )
        .get( '/api/v3/clubs/dummy_club_id' )
        .reply( 200, club );

      return strava.getClub( 'dummy_club_id', 'access_token' ).then( function fulfilled( data ) {
        should( data === club ).be.ok;
      } );
    } );

    it( 'should handle a cycling club', function () {
      var club = clubs[ 1 ];
      nock( 'https://www.strava.com' )
        .get( '/api/v3/clubs/dummy_club_id' )
        .reply( 200, club );

      return strava.getClub( 'dummy_club_id', 'access_token' ).then( function fulfilled( data ) {
        should( data === club ).be.ok;
      } );
    } );

    it( 'should handle a triathlon club', function () {
      var club = clubs[ 2 ];
      nock( 'https://www.strava.com' )
        .get( '/api/v3/clubs/dummy_club_id' )
        .reply( 200, club );

      return strava.getClub( 'dummy_club_id', 'access_token' ).then( function fulfilled( data ) {
        should( data === club ).be.ok;
      } );
    } );

    it( 'should handle an other club', function () {
      var club = clubs[ 3 ];
      nock( 'https://www.strava.com' )
        .get( '/api/v3/clubs/dummy_club_id' )
        .reply( 200, club );

      return strava.getClub( 'dummy_club_id', 'access_token' ).then( function fulfilled( data ) {
        should( data === club ).be.ok;
      } );
    } );

    it( 'should handle 400 error', function () {
      nock( 'https://www.strava.com' )
        .get( '/api/v3/clubs/dummy_club_id' )
        .reply( 400 );

      return strava.getClub( 'dummy_club_id', 'access_token' ).then( null, function rejected( error ) {
        should( error.name === 'StatusCodeError' ).be.ok;
      } );
    } );
  } );

  /**
   * Get activities from API
   */

  describe( 'Get activities', function () {
    it( 'should get activities', function () {
      var job = jobs[ 0 ];
      var utcOffset = moment().utcOffset();

      // Set each activity start_date to now so that it gets passed through the filter
      var runnableActivities = _.map( activities, function ( activity ) {
        var startDate = moment()
          .add( utcOffset, 'minutes' )
          .toISOString();
        return Object.assign( {}, activity, { jobId: job._id, start_date: startDate } );
      } );

      job.club.activity_types = [ 'Run' ];

      var runActivities = _.filter( runnableActivities, function ( activity ) {
        return activity.type === 'Run';
      } );

      nock( 'https://www.strava.com/api/v3/clubs/' + job.club.id )
        .get( '/activities' )
        .reply( 200, runnableActivities );

      return strava.getActivities( job ).then( function ( result ) {
        result.should.have.length( runActivities.length );
      } );
    } );
  } );

  /**
   * Create leaderboard
   */
  describe( 'Get leaderboard', function () {
    it( 'should create leaderboard', function () {
      var job = jobs[ 0 ];

      _.map( activities, function ( activity ) {
        return Object.assign( {}, activity, { jobId: job._id } );
      } );

      activities[ 0 ].start_date = moment().format();
      activities[ 2 ].start_date = moment().format();
      activities[ 3 ].start_date = moment().format();

      return testHelpers
        .saveActivities( activities )
        .then( function () {
          return leaderboard.getLeaderboard( job );
        } )
        .then( function ( result ) {
          should( result === leaderboards[ 0 ] ).be.ok;
        } );
    } );

    it( 'should not create leaderboard if no relevant activities', function () {
      var job = jobs[ 0 ];
      job._id = 'job_with_no_activities';

      return testHelpers
        .saveActivities( activities )
        .then( function () {
          return leaderboard.getLeaderboard( job );
        } )
        .then( function ( result ) {
          should.not.exist( result );
        } );
    } );
  } );

  /**
   * Filter activities by activity type and time
   */
  describe( 'Filter activities', function () {
    var runJob = jobs[ 0 ],
      walkJob = jobs[ 1 ];

    it( 'should show no activities', function () {
      runJob.club.activity_types = [];
      var matches = strava.filterActivities( activities, runJob );
      matches.length.should.equal( 0 );
    } );

    it( 'should show run activities', function () {
      runJob.club.activity_types = [ 'Run' ];
      var runActivities = _.filter( activities, function ( activity ) {
        return activity.type === 'Run';
      } );

      var matches = strava.filterActivities( activities, runJob );
      should( matches === runActivities ).be.ok;
    } );

    it( 'should show walk activities', function () {
      walkJob.club.activity_types = [ 'Walk' ];
      var walkActivities = _.filter( activities, function ( activity ) {
        return activity.type === 'Walk';
      } );

      var matches = strava.filterActivities( activities, walkJob );
      should( matches === walkActivities ).be.ok;
    } );
  } );
} );

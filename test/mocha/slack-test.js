// External dependencies
var should = require( 'should' ),
  mocha = require( 'mocha' ),
  _ = require( 'lodash' ),
  nock = require( 'nock' );

var describe = mocha.describe;
var it = mocha.it;

// Dependencies
var slack = require( '../../app/core/slack' ),
  slackUtils = require( '../../app/core/utils/slack' ),
  jobs = require( '../fixtures/jobs' ).jobs,
  activities = require( '../fixtures/activities' ).activities,
  leaderboards = require( '../fixtures/leaderboards' ).leaderboards,
  products = require( '../fixtures/products' ).products,
  messages = require( '../fixtures/messages' );

// Local vars
var activityMessages = messages.activityMessages,
  leaderboardMessages = messages.leaderboardMessages;

describe( 'Slack', function () {
  /**
   * Should create slack message depending on activity
   */
  describe( 'Create activity messages', function () {
    var runActivity = activities[ 0 ],
      partialRunActivity = activities[ 1 ],
      walkActivity = activities[ 7 ],
      rideActivity = activities[ 9 ];

    var runActivityMessage = activityMessages[ 0 ],
      partialRunActivityMessage = activityMessages[ 1 ];

    partialRunActivity.map.summary_polyline = null;

    it( 'should create a complete activity message', function () {
      var message = slackUtils.createActivityMessage( runActivity );
      should( message === runActivityMessage ).be.ok;
    } );

    it( 'should handle an empty activity', function () {
      var message = slackUtils.createActivityMessage( null );
      should.not.exist( message );
    } );

    it( 'should create a partial activity message', function () {
      var message = slackUtils.createActivityMessage( partialRunActivity );
      should( message === partialRunActivityMessage ).be.ok;
    } );

    describe( 'Activity type', function () {
      it( 'should create no message', function () {
        var message = slackUtils.createActivityMessage( null );
        should.not.exist( message );
      } );

      it( 'should create a run activity message', function () {
        var message = slackUtils.createActivityMessage( runActivity );
        var createdTitle = message.attachments[ 0 ].title;
        createdTitle.should.eql( 'Wet and windy morning Run' );
      } );

      it( 'should create a walk activity message', function () {
        var message = slackUtils.createActivityMessage( walkActivity );
        var createdTitle = message.attachments[ 0 ].title;
        createdTitle.should.eql( 'Afternoon Walk' );
      } );

      it( 'should create a ride activity message', function () {
        var message = slackUtils.createActivityMessage( rideActivity );
        var createdTitle = message.attachments[ 0 ].title;
        createdTitle.should.eql( 'Afternoon Ride' );
      } );
    } );

    describe( 'Activity time', function () {
      var secondsActivity = _.assign( {}, activities[ 0 ], { moving_time: 18 } ),
        minutesActivity = _.assign( {}, activities[ 1 ], { moving_time: 1800 } ),
        hoursActivity = _.assign( {}, activities[ 2 ], { moving_time: 7200 } ),
        hoursMinutesSecondsActivity = _.assign( {}, activities[ 3 ], { moving_time: 80781 } );

      it( 'should show hours and minutes with padding seconds', function () {
        var message = slackUtils.createActivityMessage( secondsActivity );
        var createdTitle = message.attachments[ 1 ].fields[ 0 ].value;
        createdTitle.should.eql( 'I ran 8.6km in 00:18' );
      } );

      it( 'should show minutes with padding seconds', function () {
        var message = slackUtils.createActivityMessage( minutesActivity );
        var createdTitle = message.attachments[ 1 ].fields[ 0 ].value;
        createdTitle.should.eql( 'I ran 3.3km in 30:00' );
      } );

      it( 'should show hours and minutes with padding seconds', function () {
        var message = slackUtils.createActivityMessage( hoursActivity );
        var createdTitle = message.attachments[ 1 ].fields[ 0 ].value;
        createdTitle.should.eql( 'I ran 5.0km in 02:00:00' );
      } );

      it( 'should show values for hours,minutes and seconds', function () {
        var message = slackUtils.createActivityMessage( hoursMinutesSecondsActivity );
        var createdTitle = message.attachments[ 1 ].fields[ 0 ].value;
        createdTitle.should.eql( 'I ran 5.8km in 22:26:21' );
      } );
    } );

    /**
     * Filter activities by activity type and time
     */
    describe( 'Activity maps', function () {
      var treadmillRun = activities[ 6 ],
        realRun = activities[ 5 ];

      it( 'should show not show a map for a treadmill run', function () {
        var message = slackUtils.createActivityMessage( treadmillRun );

        should.not.exists( message.attachments[ 1 ].image_url );
      } );

      it( 'should show show a map for a real run', function () {
        var message = slackUtils.createActivityMessage( realRun );

        message.attachments[ 1 ].image_url.should.not.be.empty;
      } );
    } );
  } );

  /**
   * Send activities
   */
  describe( 'Send activities', function () {
    var job = jobs[ 0 ];
    it( 'should send all messages successfully', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .times( activities.length )
        .reply( 200, {} );

      return slack.sendActivities( activities, job ).then( function fulfilled( completedJobs ) {
        completedJobs.length.should.eql( activities.length );
        _.each( completedJobs, function ( completedJob ) {
          completedJob.should.eql( job._id );
        } );
      } );
    } );

    it( 'should handle 404 from Slack', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .times( activities.length - 1 )
        .reply( 200, {} );

      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .reply( 404 );

      return slack.sendActivities( activities, job ).then( function fulfilled( completedJobs ) {
        completedJobs.length.should.eql( activities.length );
        _.last( completedJobs ).should.have.ownProperty( 'error' );
      } );
    } );
  } );

  /**
   * Should create leaderboard
   */
  describe( 'Create leaderboard', function () {
    var job = jobs[ 0 ];

    var leaderboard = leaderboards[ 0 ],
      emptyLeaderboard;

    var emptyLeaderboardMessage = leaderboardMessages[ 1 ],
      leaderboardMessage = leaderboardMessages[ 0 ];

    it( 'should create a leaderboard message', function () {
      var message = slackUtils.createLeaderboardMessage( leaderboard, job );

      ( message === leaderboardMessage ).should.be.ok;
    } );

    it( 'should create an empty leaderboard message', function () {
      var message = slackUtils.createLeaderboardMessage( emptyLeaderboard, job );
      ( message === emptyLeaderboardMessage ).should.be.ok;
    } );
  } );

  /**
   * Send leaderboard
   */
  describe( 'Send leaderboard', function () {
    var job = jobs[ 0 ],
      leaderboard = leaderboards[ 0 ];

    it( 'should send leaderboard successfully', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .reply( 200, {} );

      return slack.sendLeaderboard( leaderboard, job ).then( function fulfilled( completedJob ) {
        completedJob.should.eql( job._id );
      } );
    } );

    it( 'should handle 404 from Slack', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .reply( 404 );

      return slack.sendLeaderboard( leaderboard, job ).then( function fulfilled( completedJob ) {
        completedJob.should.have.ownProperty( 'error' );
      } );
    } );
  } );

  /**
   * Should create products
   */
  describe( 'Create products message', function () {
    var job = jobs[ 0 ];
    var product = products[ 0 ];
    var productMessage = messages.productsMessages[ 0 ];

    it( 'should create a products message', function () {
      var message = slackUtils.createProductsMessage( product, job );
      ( message === productMessage ).should.be.ok;
    } );
  } );

  /**
   * Send products
   */
  describe( 'Send products', function () {
    var job = jobs[ 0 ];

    it( 'should send products successfully', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .reply( 200, {} );

      return slack.sendProducts( products, job ).then( function fulfilled( completedJob ) {
        completedJob.should.eql( job._id );
      } );
    } );

    it( 'should not send product message if no products', function () {
      return slack.sendProducts( [], job ).then( function fulfilled( completedJob ) {
        completedJob.should.eql( job._id );
      } );
    } );

    it( 'should handle 404 from Slack', function () {
      nock( 'https://hooks.slack.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/services/fake_url' )
        .reply( 404 );

      return slack.sendProducts( products, job ).then( function fulfilled( completedJob ) {
        completedJob.should.have.ownProperty( 'error' );
      } );
    } );
  } );
} );

// External dependencies
var nock = require( 'nock' ),
  request = require( 'supertest' ),
  should = require( 'should' ),
  mocha = require( 'mocha' );

var describe = mocha.describe;
var it = mocha.it;

// Dependencies
var athletes = require( '../fixtures/athletes' ).athletes;

// Local variables
var app = require( '../../app' );

describe( 'Router', function () {
  /**
   * Connect to Strava
   */
  describe( 'Homepage', function () {
    it( 'should return the form to connect to Strava', function () {
      return request( app )
        .get( '/' )
        .then( function ( response ) {
          response.status.should.eql( 200 );
          response.text.should.match( /Connect with Strava/ );
        } );
    } );
  } );

  describe( 'Authentication', function () {
    it( 'should return a signed in user with a successful authorization request', function () {
      nock( 'https://www.strava.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/oauth/token' )
        .reply( 200, {
          access_token: 'dummy_access_token',
          athlete: athletes[ 0 ]
        } );

      request( app )
        .get( '/register?code=ZZZ' )
        .then( function ( response ) {
          response.status.should.eql( 200 );
          response.text.should.match( /Connected as/ );
        } );
    } );

    it( 'should return an error with an unsuccessful authorization request', function () {
      request( app )
        .get( '/register?error=access_denied' )
        .then(
          function ( response ) {
            response.status.should.eql( 200 );
            response.text.should.match( /Sorry/ );
          },
          function ( error ) {
            should( error.name === 'StatusCodeError' ).be.ok;
          }
        );
    } );

    it( 'should handle 4XX responseponse from Strava', function () {
      nock( 'https://www.strava.com' )
        .filteringRequestBody( function () {
          return '*';
        } )
        .post( '/oauth/token' )
        .reply( 401 );

      request( app )
        .get( '/register?code=ZZZ' )
        .then(
          function ( response ) {
            response.status.should.eql( 200 );
            response.text.should.match( /Sorry/ );
          },
          function ( error ) {
            should( error.name === 'StatusCodeError' ).be.ok;
          }
        );
    } );
  } );
} );

var mocha = require( 'mocha' );

var beforeEach = mocha.beforeEach;

// Dependencies
var testHelpers = require( './helpers' );

// Prime test db
beforeEach( function () {
  return testHelpers.dropDbs();
} );

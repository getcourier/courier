// External dependencies
var should = require( 'should' ),
  _ = require( 'lodash' ),
  includes = require( 'lodash.includes' ),
  mocha = require( 'mocha' );

var describe = mocha.describe;
var it = mocha.it;

// Dependencies
var db = require( '../../app/lib/db' ),
  testHelpers = require( './helpers' ),
  jobs = require( '../fixtures/jobs' ).jobs,
  activities = require( '../fixtures/activities' ).activities;

describe( 'DB', function () {
  /**
   * Save jobs to db
   */
  describe( 'Save and retrieve jobs', function () {
    it( 'should save and retrieve 2 jobs from db', function () {
      return db
        .saveJob( jobs )
        .then( db.getJobs )
        .then( function ( records ) {
          should( records === jobs ).be.ok;
        } );
    } );
  } );

  /**
   * Save activities to db
   */
  describe( 'Save activities', function () {
    var job1Id = jobs[ 0 ]._id,
      job2Id = jobs[ 1 ]._id;

    // Set each activity start_date to now so that it gets passed through the filter
    var runnableActivities = _.map( activities, function ( activity ) {
      return Object.assign( {}, activity, { jobId: job1Id } );
    } );

    var firstActivitiesBatch = runnableActivities.slice( 0, 2 ),
      secondActivitiesBatch = runnableActivities.slice( 2 );

    it( 'should save unique activities to db', function () {
      return testHelpers
        .saveActivities( firstActivitiesBatch )
        .then( function ( result ) {
          _.compact( result ).should.have.length( firstActivitiesBatch.length );
          _.forEach( result, function ( activity ) {
            includes( firstActivitiesBatch, activity ).should.be.ok;
          } );
        } )
        .then( function () {
          return testHelpers.saveActivities( secondActivitiesBatch ).then( function ( result ) {
            // Duplicate run *shouldn't* be saved
            _.compact( result ).should.have.length( secondActivitiesBatch.length - 1 );
            _.forEach( result, function ( activity ) {
              includes( secondActivitiesBatch, activity ).should.be.ok;
            } );
          } );
        } )
        .then( function () {
          return db.getActivities().then( function ( records ) {
            records.should.have.length( activities.length - 1 );
          } );
        } );
    } );

    it( 'should save duplicate runs with different jobIds to db', function () {
      // Change first activity (which is first of duplicate) to have different jobId
      firstActivitiesBatch[ 0 ].jobId = job2Id;

      return testHelpers
        .saveActivities( firstActivitiesBatch )
        .then( function ( result ) {
          _.compact( result ).should.have.length( firstActivitiesBatch.length );
          _.forEach( result, function ( activity ) {
            includes( firstActivitiesBatch, activity ).should.be.ok;
          } );
        } )
        .then( function () {
          return testHelpers.saveActivities( secondActivitiesBatch ).then( function ( result ) {
            // Duplicate run *should* be saved as it has a different job id
            _.compact( result ).should.have.length( secondActivitiesBatch.length );
            _.forEach( result, function ( activity ) {
              includes( secondActivitiesBatch, activity ).should.be.ok;
            } );
          } );
        } )
        .then( function () {
          return db.getActivities().then( function ( records ) {
            records.should.have.length( activities.length );
          } );
        } );
    } );
  } );

  /**
   * Remove job from db
   */
  describe( 'Remove job', function () {
    it( 'should remove a single job by ID from db', function () {
      var jobToRemove = jobs[ 0 ];

      return testHelpers
        .primeJobsCollection()
        .then( function () {
          db.removeJob( jobToRemove._id.toString() );
        } )
        .then( db.getJobs )
        .then( function ( result ) {
          includes( result, jobToRemove ).should.not.be.ok;
          result.should.have.length( jobs.length - 1 );
        } );
    } );
  } );
} );

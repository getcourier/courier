// External dependencies
var _ = require( 'lodash' ),
  moment = require( 'moment' );

// Dependencies
var environment = require( '../lib/environment' ),
  db = require( '../lib/db' ),
  utils = require( './utils/jobs' );

/**
 * Get leaderboard stats
 */
function getLeaderboard( job ) {
  var period = moment.duration( utils.getStravaPeriod( environment.jobs.leaderboardFrequency ) );
  var startOfPeriod = moment()
    .subtract( period )
    .format();
  var query = {
    $and: [
      { jobId: job._id.toString() },
      {
        start_date: {
          $gte: startOfPeriod
        }
      }
    ]
  };

  return db.getActivities( query ).then( handleLeaderboardActivities );
}

/**
 * Create stats now we've got all the records
 */
function handleLeaderboardActivities( activities ) {
  if ( !activities.length ) {
    return null;
  }

  // Set up leaderboard
  var leaderboard = {};

  // Set up stats we want to track
  var stats = [
    // 'numberOfActivities',
    'longestDistance',
    'totalDistance',
    // 'highestElevation',
    'totalClimb'
  ];

  // Group activities by athlete then get stats for each athlete
  var athleteStats = _( activities )
    .groupBy( getAthleteName )
    .map( getStatsForAthlete )
    .value();

  // For each stat we want, get the athletes in order
  _.forEach( stats, function ( stat ) {
    leaderboard[ stat ] = _( athleteStats )
      .map( function ( athlete ) {
        return { name: athlete.name, avatar: athlete.avatar, value: athlete[ stat ] };
      } )
      .orderBy( 'value', 'desc' )
      .take( 3 )
      .value();
  } );

  return leaderboard;
}

/**
 * Condense all the activities for an athlete to a stats summary
 */
function getStatsForAthlete( athlete, key ) {
  var base = {
    name: '',
    avatar: '',
    // numberOfActivities: 0,
    longestDistance: 0,
    totalDistance: 0,
    // highestElevation: 0,
    totalClimb: 0
  };

  // Reduce the activities down to a summary
  return _.reduce(
    athlete,
    function ( stats, activity ) {
      return {
        name: key,
        avatar: activity.athlete.profile,
        // numberOfActivities: stats.numberOfActivities + 1,
        longestDistance: Math.max( stats.longestDistance, activity.distance ),
        totalDistance: stats.totalDistance + activity.distance,
        // highestElevation: Math.max( stats.highestElevation, activity.total_elevation_gain ),
        totalClimb: stats.totalClimb + activity.total_elevation_gain
      };
    },
    base
  );
}

/**
 * Group activities by athlete
 */
function getAthleteName( activity ) {
  return activity.athlete.firstname + ' ' + activity.athlete.lastname;
}

// Exports
module.exports = {
  getLeaderboard: getLeaderboard
};

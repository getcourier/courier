// External dependencies
var _ = require( 'lodash' ),
    request = require( 'request-promise' );

// Dependencies
var environment = require( '../lib/environment' ),
    logger = require( '../lib/logger' );

/**
 * Get products from store
 */
function getProducts( job ) {
  var params = {};
  var activityTypes = job.club.activity_types;

  // If there's only one activity type, send it
  // Else send nothing and API will return products for any activity type
  if ( activityTypes.length === 1 ) {
    params.activity = activityTypes[ 0 ];
  }

  var options = {
    url: environment.products.url + '/products',
    qs: params,
    json: true
  };

  // Make request
  return request( options )
    .then( handleProducts )
    .catch( function ( error ) {
      logger.error( 'handleProductsError', { url: options.url, error: error.message } );
      return null;
    } );
}

/**
 * Handle (empty) products
 */
function handleProducts( response ) {
  var products = response.products;
  if ( !products || _.isEmpty( products ) ) {
    return null;
  }

  return _.sampleSize( products, environment.products.quantity );
}

// Exports
module.exports = {
  getProducts: getProducts
};

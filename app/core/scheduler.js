var CronJob = require( 'cron' ).CronJob;

// Dependencies
var jobs = require( './jobs' ),
    utils = require( './utils/jobs' ),
    environment = require( '../lib/environment' );

/**
 * Set up cron jobs
 */
function scheduleJobs() {
  // Set up activities cron job
  scheduleJob( {
    frequency: utils.getFrequency( environment.jobs.activitiesFrequency ),
    job: jobs.runActivitiesJobs
  } );

  // Set up activities cron job
  scheduleJob( {
    frequency: utils.getFrequency( environment.jobs.productsFrequency ),
    job: jobs.runAdvertisingJobs
  } );

  // Set up leaderboard cron job
  scheduleJob( {
    frequency: utils.getFrequency( environment.jobs.leaderboardFrequency ),
    job: jobs.runLeaderboardJobs
  } );
}

function scheduleJob( params ) {
  return new CronJob( {
    cronTime: params.frequency,
    onTick: params.job,
    start: true,
    timeZone: 'Europe/London'
  } );
}

module.exports = {
  scheduleJobs: scheduleJobs
};
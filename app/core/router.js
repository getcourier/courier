// External dependencies
var express = require( 'express' ),
  _ = require( 'lodash' );

// Dependencies
var environment = require( '../lib/environment' ),
  middleware = require( '../lib/middleware' ),
  logger = require( '../lib/logger' ),
  jobs = require( './jobs' ),
  slack = require( './slack' ),
  strava = require( './strava' ),
  athlete = require( './athlete' );

// Local variables
var router = new express.Router();

// Set up routes
router.get( '/', showSlackForm );
router.get( '/register', getSlackAccessToken );
router.get( '/strava', getStravaAccessToken );
router.get( '/link', getStravaAccessTokenForAthlete );
router.post( '/success', handleSignup );
router.get( '/leaderboards', middleware.auth, runLeaderboards );
router.get( '/activities', middleware.auth, runActivities );
router.get( '/products', middleware.auth, runProducts );
router.get( '/privacy-policy', showPrivacyPolicy );

var templateEnvVars = {
  stravaAppId: environment.strava.appId,
  slackClientId: environment.slack.clientId,
  baseUrl: environment.baseUrl
};

/**
 * Show privacy policy
 */
function showPrivacyPolicy( request, response ) {
  response.render( 'privacy' );
}

/**
 * Show the basic 'Connect with Strava' form
 */
function showSlackForm( request, response ) {
  var templateVars = {
    environment: templateEnvVars
  };
  response.render( 'home', templateVars );
}

/**
 * Get Slack access token with 'Add to Slack' button
 */
function getSlackAccessToken( request, response ) {
  var slackError = request.query.error;
  if ( slackError ) {
    logger.error( 'getSlackAccessToken', { error: slackError } );
    if ( slackError === 'access_denied' ) {
      return response.render(
        'strava',
        _.assign( {}, { error: slackError, environment: templateEnvVars } )
      );
    }
    return response.render( 'error', { error: slackError } );
  }

  var params = request.query,
    authCode = params.code;

  return slack
    .getAccessToken( authCode )
    .then( function handleSlackAccessToken( data ) {
      logger.info( 'Slack signup' );
      return jobs.createJob( { slack: data } );
    } )
    .then( function handleCreateJob( record ) {
      logger.info( 'Job created', record._id.toString() );
      var templateVars = _.assign( {}, record, { environment: templateEnvVars } );
      return response.render( 'strava', templateVars );
    } )
    .catch( function ( error ) {
      logger.error( 'handleSlackAccessToken', error );
      return response.render( 'error', { error: error.message } );
    } );
}

/**
 * Get Strava access token when Strava sends auth code
 */
function getStravaAccessToken( request, response ) {
  var stravaError = request.query.error;
  if ( stravaError ) {
    logger.error( 'getStravaAccessToken', { error: stravaError.message } );
    return response.render( 'error', { error: stravaError } );
  }

  var authCode = request.query.code,
    jobId = request.query.state;

  var viewHelpers = {
    eqls: function ( v1, v2, options ) {
      return v1 === v2 ? options.fn( this ) : options.inverse( this );
    }
  };

  return strava
    .getAccessToken( authCode )
    .then( function handleStravaAccessToken( data ) {
      return jobs.updateJob( jobId, { strava: data.strava, athlete: data.athlete } );
    } )
    .then( function handleUpdateJob( record ) {
      logger.info( 'Strava signup', record._id.toString() );
      var templateVars = _.assign( {}, record, { environment: templateEnvVars }, viewHelpers );
      return response.render( 'strava', templateVars );
    } )
    .catch( function ( error ) {
      logger.error( 'handleStravaAccessToken', error );
      return response.render( 'error', { error: error.message } );
    } );
}

/**
 * Get Strava access token when Strava sends auth code
 */
function getStravaAccessTokenForAthlete( request, response ) {
  var stravaError = request.query.error;
  if ( stravaError ) {
    logger.error( 'getStravaAccessToken', { error: stravaError.message } );
    return response.render( 'error', { error: stravaError } );
  }

  var authCode = request.query.code,
    slackId = request.query.state,
    accessToken;

  return strava
    .getAccessToken( authCode )
    .then( function handleStravaAccessToken( data ) {
      accessToken = data.access_token;
      return athlete.saveAthlete( _.assign( {}, data.athlete, { slackId: slackId } ) );
    } )
    .then( function handleSaveAthlete( record ) {
      logger.info( 'Strava individual signup', record._id.toString() );
      var templateVars = _.assign( {}, record, {
        accessToken: accessToken,
        environment: templateEnvVars
      } );
      return response.render( 'link', templateVars );
    } )
    .catch( function ( error ) {
      logger.error( 'handleStravaAccessToken', error );
      return response.render( 'error', { error: error.message } );
    } );
}

/**
 * Handle signup
 */
function handleSignup( request, response ) {
  var params = request.body,
    jobId = params.jobId,
    clubId = params.stravaClubId;

  return jobs
    .updateJob( jobId, { club: { id: clubId } } )
    .then( function handleUpdateJob( job ) {
      // Show success page
      logger.info( 'Job created!', job );
      var templateVars = _.assign( {}, job, { environment: templateEnvVars } );
      response.render( 'success', templateVars );
      return job;
    } )
    .then( function runBackgroundTasks( job ) {
      // Get club and team members here
      return strava.getClub( clubId, job.strava.accessToken );
    } )
    .then( function handleStravaClub( club ) {
      // Set active flag when we have everything so this job will run
      logger.info( 'Job is live!' );
      return jobs.updateJob( jobId, { club: club, active: true, cohort: 1 } );
    } )
    .catch( function ( error ) {
      logger.error( 'handleSignup', error );
      return response.render( 'error', { error: error.message } );
    } );
}

/**
 * Link Strava profile to Slack user
 */
// eslint-disable-next-line no-unused-vars
function linkStravaProfile( request, response ) {
  var stravaError = request.query.error;
  if ( stravaError ) {
    logger.error( 'linkStravaProfile', { error: stravaError.message } );
    return response.render( 'error', { error: stravaError } );
  }

  // eslint-disable-next-line no-unused-vars
  var authCode = request.query.code,
    // eslint-disable-next-line no-unused-vars
    jobId = request.query.state;

  return strava.saveAthlete( response ).catch( function ( error ) {
    logger.error( 'handleSignup', error );
    return response.render( 'error', { error: error.message } );
  } );
}

/**
 * Run activities
 */
function runActivities( request, response ) {
  return runJob( response, jobs.runActivitiesJobs );
}

/**
 * Run the leaderboard
 */
function runLeaderboards( request, response ) {
  return runJob( response, jobs.runLeaderboardJobs );
}

/**
 * Run products
 */
function runProducts( request, response ) {
  return runJob( response, jobs.runProductsJobs );
}

/**
 * Run something
 */
function runJob( response, job ) {
  return job()
    .then( function ( results ) {
      return response.render( 'success', results );
    } )
    .catch( function ( error ) {
      return response.render( 'error', { error: error } );
    } );
}

// Exports
module.exports = router;

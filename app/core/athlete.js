// External dependencies
var _ = require( 'lodash' );

// Dependencies
var db = require( '../lib/db' );

/**
 * Create athlete
 */
function saveAthlete( data ) {
  // Save to db
  return db.saveAthlete( data );
}

/**
 * Update athlete
 */
function updateAthlete( athleteId, data ) {
  // Save to db
  return db.getAthlete( athleteId ).then( function ( record ) {
    var newData = _.merge( {}, record, data );
    return db.updateAthlete( athleteId, newData );
  } );
}

module.exports = {
  saveAthlete: saveAthlete,
  updateAthlete: updateAthlete
};

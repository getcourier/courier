// External dependencies
var _ = require( 'lodash' ),
  request = require( 'request-promise' ),
  puppeteer = require( 'puppeteer' );

// Dependencies
var environment = require( '../lib/environment' ),
  logger = require( '../lib/logger' ),
  utils = require( './utils/strava' );

/**
 * Get access token from Strava
 */
function getAccessToken( authCode ) {
  var options = {
    url: 'https://www.strava.com/oauth/token',
    json: true,
    body: {
      client_id: environment.strava.appId,
      client_secret: environment.strava.appSecret,
      code: authCode
    }
  };

  // Post request
  return request.post( options ).then( function handleGetAccessToken( body ) {
    return {
      strava: {
        accessToken: body.access_token
      },
      athlete: body.athlete
    };
  } );
}

/**
 * Get club data from Strava
 */
function getClub( clubId, accessToken ) {
  var options = {
    url: 'https://www.strava.com/api/v3/clubs/' + clubId,
    json: true,
    auth: {
      bearer: accessToken
    }
  };

  // Make request
  return request.get( options ).then( function handleGetClub( data ) {
    return _.assign( {}, data, { activity_types: utils.getActivityTypes( data.sport_type ) } );
  } );
}

/**
 * Get activities from Strava
 */
function getActivities( job ) {
  var options = {
    url: 'https://www.strava.com/api/v3/clubs/' + job.club.id + '/activities',
    params: { per_page: 30 },
    json: true,
    auth: {
      bearer: job.strava.accessToken
    }
  };

  // Make request
  return request( options )
    .then( function ( activities ) {
      // Filter activities to the correct activity type
      return filterActivities( activities, job );
    } )
    .then( function ( activities ) {
      // Add jobId to activities
      return augmentActivities( activities, job );
    } )
    .catch( function ( error ) {
      console.error( 'strava.handleActivitiesError', { url: options.url, error: error.message } );
      throw error;
    } );
}

/**
 * Filter activities based on activity type (specified in job)
 */
function filterActivities( activities, job ) {
  return _.filter( activities, function ( activity ) {
    return job.club.activity_types.indexOf( activity.type ) >= 0;
  } );
}

/**
 * Add jobId to activities
 */
function augmentActivities( activities, job ) {
  return activities.map( function ( activity ) {
    return _.assign( {}, activity, { jobId: job._id.toString(), webhookUrl: job.slack.webhook.url } );
  } );
}

/**
 * Get detailed activities (and so photos, stats etc)
 */
function getPhotos( activity ) {
  if ( !activity.total_photo_count ) {
    return null;
  }

  // TODO: Handle photo failure (ignore errors)
  return scrapeStrava( activity )
    .then( function ( photos ) {
      if ( !photos || !photos.length ) {
        logger.info(
          'strava.getPhotos',
          activity.name,
          'Should have photo but not on activity object'
        );
        return null;
      }

      logger.info( 'strava.getPhotos', activity.name, photos.length + ' photos found' );
      return photos;
    } )
    .catch( function ( error ) {
      logger.error( 'strava.getPhotos', activity.name, error );
      return Promise.resolve( null );
    } );
}

function scrapeStrava( activity ) {
  return puppeteer.launch().then( function ( browser ) {
    browser.newPage().then( function ( page ) {
      page
        .goto( 'https://www.strava.com/activities/' + activity.id )
        .waitForSelector( '#activity-photos-container' )
        .evaluate( function () {
          var thumbnails = document.querySelectorAll( '#activity-photos li img' );
          if ( !thumbnails.length ) {
            return null;
          }
          return _.map( thumbnails, function ( thumbnail ) {
            return thumbnail.src;
          } );
        } )
        .tap( function end( result ) {
          logger.info( 'strava.scrapeStrava', result );
          puppeteer.close();
        } );
    } );
  } );
}

function getCompositePhoto( activity ) {
  if ( !activity.photos ) {
    return null;
  }

  return request( environment.photos.url + '/compose' )
    .qs( {
      id: activity.id,
      photo: activity.photos[ 0 ],
      polyline: activity.map.summary_polyline
    } )
    .then( function handleCompositePhoto( photoUrl ) {
      logger.info( 'strava.getCompositePhoto', activity.name, photoUrl );
      return photoUrl;
    } )
    .catch( function ( error ) {
      logger.error( 'strava.getCompositePhoto', activity.name, error.message );
      return Promise.resolve( null );
    } );
}

// Exports
module.exports = {
  getAccessToken: getAccessToken,
  getClub: getClub,
  getPhotos: getPhotos,
  getCompositePhoto: getCompositePhoto,
  getActivities: getActivities,
  filterActivities: filterActivities
};

// External dependencies
var Promise = require( 'bluebird' ),
    _ = require( 'lodash' );

// Dependencies
var slack = require( './slack' ),
    strava = require( './strava' ),
    leaderboard = require( './leaderboard' ),
    products = require( './products' ),
    db = require( '../lib/db' ),
    logger = require( '../lib/logger' );

/**
 * Create job
 */
function createJob( data ) {
  // Save to db
  return db.saveJob( data );
}

/**
 * Update job
 */
function updateJob( jobId, data ) {
  // Save to db
  return db.getJob( jobId )
    .then( function ( record ) {
      var newData = _.merge( {}, record, data );
      return db.updateJob( jobId, newData );
    } );
}

/**
 * Run activities jobs
 */
function runActivitiesJobs() {
  var logStartTime = new Date().getTime();

  logger.info( 'Kicking off activity jobs', new Date() );
  return db.getActiveJobs()
    .then( getActivitiesForJobs )
    .then( createActivityQueue )
    .then( processActivityQueue )
    .then( function ( result ) {
      var timeTaken = ( new Date().getTime() - logStartTime ) / 1000;
      logger.info( 'Finished activities jobs', _.compact( result ).length + ' activities added in ' + timeTaken + 's' );
    } )
    .catch( logger.error );
}

/**
 * Run leaderboards jobs
 */
function runLeaderboardJobs() {
  logger.info( 'Kicking off leaderboard jobs', new Date() );
  return db.getActiveJobs()
    .then( processLeaderboardJobs )
    .tap( function ( result ) {
      logger.debug( 'Finished leaderboard jobs', result );
    } )
    .catch( logger.error );
}

/**
 * Run products jobs
 */
function runProductsJobs() {
  logger.info( 'Kicking off products jobs', new Date() );
  return db.getProductJobs()
    .then( processProductsJobs )
    .tap( function ( result ) {
      logger.info( 'Finished product jobs', _.compact( result ).length + ' records processed' );
    } )
    .catch( logger.error );
}

/**
 * Get activities for a job
 */
function getActivitiesForJobs( jobs ) {
  return Promise.mapSeries( jobs, getActivitiesForJob );
}

function getActivitiesForJob( job ) {
  logger.info( 'Running activities job', job.slack.team.name, job.slack.webhook.channel );

  return strava.getActivities( job )
    .catch( function ( error ) {
      logger.error( 'jobs.handleActivitiesJobError', error.message, job._id );
      handleActivitiesJobError( error, job );
      throw error;
    } );
}

function createActivityQueue( activitiesByJob ) {
  return _.flatten( activitiesByJob );
}

function processActivityQueue( queue ) {
  return Promise.map( queue, processActivityQueueItem );
}

function processActivityQueueItem( activity ) {
  return db.saveActivity( activity )
    .then( strava.getPhotos )
    .then( function ( photos ) {
      // If there are photos add them to activity object
      if ( photos ) {
        var updatedActivity = _.merge( {}, activity, photos );
        console.log( updatedActivity );
        updatedActivity.photos = photos;
        return db.updateActivity( updatedActivity );
      }
      return activity;
    } )
    .then( slack.sendActivity )
    .tap( function () {
      logger.info( 'processActivityQueueItem', activity.name, 'Activity processed' );
    } )
    .catch( function ( error ) {
      // If we've already got this activity, let it slide…
      if ( error.code === 11000 ) {
        logger.debug( 'jobs.processActivityQueueItem', activity.name, 'Activity exists' );
        return null;
      }
      logger.error( 'jobs.processActivityQueueItem', error );
      throw error;
    } );
}


/**
 * Get leaderboard for a job
 */
function processLeaderboardJobs( jobs ) {
  return Promise.map( jobs, function processLeaderboardJob( job ) {
    logger.info( 'Running leaderboard job', job.slack.team.name, job.slack.webhook.channel );

    return leaderboard.getLeaderboard( job )
      .then( function ( stats ) {
        return slack.sendLeaderboard( stats, job );
      } );
  } );
}

/**
 * Get products for a job
 */
function processProductsJobs( jobs ) {
  return Promise.map( jobs, function processProductsJob( job ) {
    logger.info( 'Running products job', job.slack.team.name, job.slack.webhook.channel );

    return products.getProducts( job )
      .then( function ( items ) {
        if ( !items || !items.length ) { return null; }
        return slack.sendProducts( items, job );
      } );
  } );
}

/**
 * Handle job error
 */
function handleActivitiesJobError( error, job ) {
  if ( error.statusCode === 401 ) {
    var message = 'Looks like there was an error authenticating your Strava group. Please try re-connecting your club at <getcourier.io>';
    return slack.sendErrorMessage( message, job.slack.team.name, job.slack.webhook.channel )
      .then( function () {
        return db.removeJob( job );
      } );
  }
  return null;
}

module.exports = {
  runActivitiesJobs: runActivitiesJobs,
  runProductsJobs: runProductsJobs,
  runLeaderboardJobs: runLeaderboardJobs,
  createJob: createJob,
  updateJob: updateJob
};

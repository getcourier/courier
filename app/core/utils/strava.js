/**
 * Get activity types based on club/sport type
 */
function getActivityTypes( sportType ) {
  var activityTypes;

  switch ( sportType ) {
    case 'running':
      activityTypes = [ 'Run' ];
      break;
    case 'cycling':
      activityTypes = [ 'Ride' ];
      break;
    case 'triathlon':
      activityTypes = [ 'Run', 'Ride', 'Swim' ];
      break;
    default:
      activityTypes = [ 'Ride', 'Kitesurf', 'Run', 'NordicSki', 'Swim', 'RockClimbing', 'Hike', 'RollerSki', 'Walk', 'Rowing', 'AlpineSki', 'Snowboard', 'BackcountrySki', 'Snowshoe', 'Canoeing', 'StairStepper', 'Crossfit', 'StandUpPaddling', 'EBikeRide', 'Surfing', 'Elliptical', 'VirtualRide', 'IceSkate', 'WeightTraining', 'InlineSkate', 'Windsurf', 'Kayaking', 'Workout', 'Yoga' ];
  }

  return activityTypes;
}
module.exports = {
  getActivityTypes: getActivityTypes
};


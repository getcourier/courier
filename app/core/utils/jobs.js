/**
 * Get frequency to kick off cron job
 *  second         0-59
 *  minute         0-59
 *  hour           0-23
 *  day of month   1-31
 *  month          0-11 (or names)
 *  day of week    0-6 (0 is Sun, or use names)
 */
function getFrequency( frequency ) {
  var frequencies = {
    second: '* * * * * *', // Every second
    minute: '0 * * * * *', // Every minute
    hour: '0 0 * * * *', // Every hour
    day: '0 0 0 * * 0-6', // Every day of the week at midnight
    biweekly: '0 0 0 * * 1,4', // Every week on Monday and Thursday
    week: '0 0 0 * * 0', // Every Sunday at midnight
    fortnight: '0 0 0 1,15 * *', // Every 1st and 15th of the month
    month: '0 0 0 1 * *' // Every 1st of the month
  };

  return frequencies[ frequency ] || frequencies.hour;
}

/**
 * Get period to fetch from Strava
 */
function getStravaPeriod( frequency ) {
  var periods = {
    second: { seconds: 1 },
    minute: { minutes: 1 },
    hour: { hours: 1 },
    day: { days: 1 },
    week: { weeks: 1 },
    month: { months: 1 }
  };

  return periods[ frequency ] || periods.hour;
}

module.exports = {
  getFrequency: getFrequency,
  getStravaPeriod: getStravaPeriod
};
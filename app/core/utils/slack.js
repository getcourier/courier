var _ = require( 'lodash' ),
  moment = require( 'moment' );

require( 'moment-duration-format' );

// Dependencies
var environment = require( '../../lib/environment' );

var activityAttachmentColor = '#fc4c02'; // Strava colour
var secondaryAttachmentColor = '#dcdcdc'; // Grey colour
var errorAttachmentColor = '#D32F2F'; // Error colour

/**
 * Create activity message
 */
function createActivityMessage( activity ) {
  if ( activity === null ) {
    return null;
  }

  var athlete = activity.athlete;

  // Strings
  var athleteName = athlete.firstname,
    athletePic = athlete.profile,
    distance = ( activity.distance / 1000 ).toFixed( 1 ) + 'km',
    movingTime = getFormattedDuration( activity.moving_time ),
    averagePace =
      moment.duration( 100 / 6 / activity.average_speed, 'minutes' ).format() + ' mins/km',
    elevation = activity.total_elevation_gain + 'm',
    pastSimpleVerb = getPastSimpleVerb( activity.type );

  // Create attachment to display description
  var descriptionAttachment = {
    color: activityAttachmentColor,
    title: activity.name,
    title_link: 'https://www.strava.com/activities/' + activity.id,
    author_name: athleteName,
    author_link: 'http://strava.com/athletes/' + athlete.id,
    author_icon: athletePic,
    fallback: athleteName + ' ' + pastSimpleVerb + ' ' + distance + ' in ' + movingTime,
    mrkdwn_in: [ 'text' ]
  };

  // Create attachment to display stats
  var statsAttachment = {
    color: secondaryAttachmentColor,
    fields: [
      { title: 'Distance', value: 'I ' + pastSimpleVerb + ' ' + distance + ' in ' + movingTime },
      { title: 'Average pace', value: averagePace, short: true },
      { title: 'Elevation', value: elevation, short: true }
    ],
    fallback: 'Average pace was ' + averagePace + '. Total elevation was ' + elevation + '.'
  };

  if ( activity.photos ) {
    statsAttachment.image_url = activity.photos[ 0 ];
  } else if ( activity.map.summary_polyline ) {
    // Add map if not a treadmill run
    statsAttachment.image_url =
      'http://maps.googleapis.com/maps/api/staticmap?style=lightness:40|saturation:-100|visibility:simplified|weight:1.0&style=element:labels.icon|visibility:off&size=1600x400&scale=2&path=weight:6|color:0xfc4c02ff|enc:' +
      activity.map.summary_polyline +
      '&key=' +
      environment.googleMapsApiKey;
  }

  return {
    attachments: [ descriptionAttachment, statsAttachment ],
    callback_id: activity.id,
    icon_url: athletePic,
    username: athleteName + ' on Strava (via #courier)'
  };
}

/**
 * Create leaderboard message
 */
function createLeaderboardMessage( leaderboard, job ) {
  // Attachments
  var attachments = [];

  if ( !leaderboard ) {
    attachments = [
      {
        color: activityAttachmentColor,
        title: 'No activity this week?',
        text: 'Shame on you all…',
        fallback: 'Shame on you all…',
        mrkdwn_in: [ 'text' ]
      }
    ];
  }

  _.forEach( leaderboard, function ( stat, key ) {
    attachments.push( {
      color: activityAttachmentColor,
      thumb_url: stat[ 0 ].avatar,
      title: makeSentenceCase( key ),
      text: getStatText( stat, key ),
      fallback: '',
      mrkdwn_in: [ 'text' ]
    } );
  } );

  return {
    text: 'See the whole leaderboard on <https://www.strava.com/clubs/' + job.club.id + '|Strava>',
    username: 'Weekly leaderboard (via #courier)',
    icon_url: environment.baseUrl + '/ico/courier-favicon-850.png',
    attachments: attachments
  };
}

/**
 * Create products message
 */
function createProductsMessage( products ) {
  // Attachments
  var attachments = [];

  _.forEach( products, function ( product ) {
    attachments.push( {
      color: activityAttachmentColor,
      thumb_url: product.image,
      title: '<' + product.link + '|' + product.title + '>',
      text: product.description,
      fallback: product.description
    } );
  } );

  return {
    text: 'Help us maintain #courier by buying a little something below…',
    username: '#courier',
    icon_url: environment.baseUrl + '/ico/courier-favicon-850.png',
    attachments: attachments
  };
}

/**
 * Create error message
 */
function createErrorMessage( error ) {
  var errorAttachment = {
    color: errorAttachmentColor,
    title: 'Oops!',
    text: error,
    fallback: error,
    fields: []
  };

  return {
    username: '#courier',
    icon_url: environment.baseUrl + '/ico/courier-favicon-850.png',
    attachments: [ errorAttachment ]
  };
}

/**
 * Make string sentence-case
 */
function makeSentenceCase( string ) {
  return string.replace( /^[a-z]|[A-Z]/g, function ( v, i ) {
    return i === 0 ? v.toUpperCase() : ' ' + v.toLowerCase();
  } );
}

function getStatText( stat, key ) {
  return _.reduce(
    stat,
    function ( text, statLine, index ) {
      var value = formatStat( statLine, key );
      var line = statLine.name + ': ' + value;
      var copy = text;

      if ( index === 0 ) {
        copy += '*' + line + '*:sports_medal: \n';
      } else {
        copy += line + '\n';
      }
      return copy;
    },
    ''
  );
}

/**
 * Format statistics
 */
function formatStat( stat, key ) {
  var value = stat.value;

  if ( key.indexOf( 'Distance' ) >= 0 ) {
    value = ( value / 1000 ).toFixed( 1 ) + 'km';
  } else if ( key.indexOf( 'Climb' ) >= 0 ) {
    value = value.toFixed( 1 ) + 'm';
  }

  return value;
}

/**
 * Format duration
 */
function getFormattedDuration( duration ) {
  var format = 'hh:mm:ss';
  if ( duration < 3600 ) {
    format = 'mm:ss';
  }

  return moment.duration( duration, 'seconds' ).format( format, { trim: false } );
}

/**
 * Get past simple verb based on activity type
 */
function getPastSimpleVerb( activityType ) {
  var pastSimpleVerb = '';

  switch ( activityType.toLowerCase() ) {
    case 'run':
      pastSimpleVerb = 'ran';
      break;
    case 'ride':
      pastSimpleVerb = 'rode';
      break;
    case 'walk':
      pastSimpleVerb = 'walked';
      break;
    default:
      pastSimpleVerb = 'managed';
  }

  return pastSimpleVerb;
}

module.exports = {
  createActivityMessage: createActivityMessage,
  createLeaderboardMessage: createLeaderboardMessage,
  createProductsMessage: createProductsMessage,
  createErrorMessage: createErrorMessage,
  getFormattedDuration: getFormattedDuration
};

// External dependencies
var request = require( 'request-promise' ),
    Promise = require( 'bluebird' );

// Dependencies
var environment = require( '../lib/environment' ),
    utils = require( './utils/slack' ),
    logger = require( '../lib/logger' );

/**
 * Get access token from Slack
 */
function getAccessToken( authCode ) {
  var options = {
    url: 'https://slack.com/api/oauth.access',
    json: true,
    form: {
      client_id: environment.slack.clientId,
      client_secret: environment.slack.clientSecret,
      code: authCode
    }
  };

  // Post request
  return request.post( options )
    .then( function handleGetAccessToken( body ) {
      if ( !body.ok ) {
        throw new Error( body.error );
      }

      return {
        accessToken: body.access_token,
        team: {
          id: body.team_id,
          name: body.team_name
        },
        webhook: {
          url: body.incoming_webhook.url,
          channel: body.incoming_webhook.channel
        }
      };
    } );
}

/**
 * Send all activities to Slack
 */
function sendActivities( activities, job ) {
  return Promise.map( activities, function ( activity ) {
    var message = utils.createActivityMessage( activity );
    return sendMessage( message, job );
  } );
}

/**
 * Send all activity to Slack
 */
function sendActivity( activity ) {
  var message = utils.createActivityMessage( activity );
  return sendMessage( message, { _id: activity.jobId, slack: { webhook: { url: activity.webhookUrl } } } );
}

/**
 * Send leaderboard to Slack
 */
function sendLeaderboard( leaderboard, job ) {
  var message = utils.createLeaderboardMessage( leaderboard, job );
  return sendMessage( message, job );
}

/**
 * Send products to Slack
 */
function sendProducts( products, job ) {
  if ( !products || !products.length ) {
    logger.debug( 'No products found for this job', job );
    return Promise.resolve( job._id );
  }
  var message = utils.createProductsMessage( products, job );
  return sendMessage( message, job );
}

/**
 * Send authentication error message to Slack
 */
function sendErrorMessage( error, job ) {
  var message = utils.createErrorMessage( error, job );
  return sendMessage( message, job );
}

/**
 * Send message to Slack
 */
function sendMessage( message, job ) {
  return request( {
    url: job.slack.webhook.url,
    method: 'post',
    json: true,
    resolveWithFullResponse: true,
    body: message
  } )
    .then( function () {
      return job._id.toString();
    } )
    .catch( function ( error ) {
      logger.error( 'sendMessage', { url: job.slack.webhook.url, error: error.message } );
      return error;
    } );
}

// Exports
module.exports = {
  getAccessToken: getAccessToken,
  sendActivities: sendActivities,
  sendActivity: sendActivity,
  sendLeaderboard: sendLeaderboard,
  sendProducts: sendProducts,
  sendErrorMessage: sendErrorMessage
};

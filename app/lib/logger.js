// External dependencies
var winston = require( 'winston' );

// Dependencies
var environment = require( './environment' );

require( 'winston-papertrail' ).Papertrail;

if ( environment.name === 'production' ) {
  winston.add( winston.transports.Papertrail, {
    colorize: true,
    host: 'logs4.papertrailapp.com',
    port: 50899
  } );
}

module.exports = winston;
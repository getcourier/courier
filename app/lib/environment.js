// External dependencies
var dotenv = require( 'dotenv' );

var env = process.env,
    environmentName = env.NODE_ENV || env.node_env || 'development',
    environment;

if ( environmentName === 'development' ) {
  dotenv.load();
}

environment = {
  project: 'courier',
  name: environmentName,
  baseUrl: env.BASE_URL,
  port: env.PORT || env.port,
  auth: {
    username: env.AUTH_USERNAME,
    password: env.AUTH_PASSWORD
  },
  googleMapsApiKey: env.GOOGLE_MAPS_API_KEY,
  mongoUrl: env.MONGO_URL,
  jobs: {
    activitiesFrequency: env.POLL_FREQUENCY || 'hour',
    productsFrequency: env.PRODUCTS_FREQUENCY || 'biweekly',
    leaderboardFrequency: env.LEADERBOARD_FREQUENCY || 'week'
  },
  photos: {
    url: env.IMAGES_SERVICE_URL
  },
  products: {
    url: env.PRODUCTS_URL,
    quantity: env.PRODUCTS_QUANTITY || 3,
  },
  strava: {
    appId: env.STRAVA_APP_ID,
    appSecret: env.STRAVA_APP_SECRET,
    color: '#fc4c02'
  },
  slack: {
    clientId: env.SLACK_CLIENT_ID,
    clientSecret: env.SLACK_CLIENT_SECRET
  }
};

// Exports
module.exports = environment;
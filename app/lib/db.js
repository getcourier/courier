// External dependencies
var Promise = require( 'bluebird' ),
  mongojs = require( 'mongojs' ),
  mongojsCollection = require( 'mongojs/lib/collection' );

// Dependencies
var environment = require( './environment' ),
  logger = require( './logger' );

Promise.promisifyAll( [ mongojsCollection ] );

var db;

/**
 * Init
 */
function init() {
  db = mongojs( environment.mongoUrl + '?authMechanism=SCRAM-SHA-1', [ 'jobs', 'activities' ] );
  db.collection( 'activities' ).createIndex(
    {
      id: 1,
      jobId: 1
    },
    {
      unique: true,
      name: 'unique_activity_per_job'
    }
  );
}

/**
 * Save athlete to db
 */
function saveAthlete( athlete ) {
  return save( 'athletes', athlete ).then( function ( record ) {
    if ( !record ) {
      throw new Error( { message: 'No athlete added' } );
    }
    return record;
  } );
}

/**
 * Update athlete in db
 */
function updateAthlete( id, data ) {
  return update( 'athletes', { _id: mongojs.ObjectId( id ) }, data );
}

/**
 * Save job to db
 */
function saveJob( job ) {
  return save( 'jobs', job ).then( function ( record ) {
    if ( !record ) {
      throw new Error( { message: 'No job added' } );
    }
    return record;
  } );
}

/**
 * Update job in db
 */
function updateJob( id, data ) {
  return update( 'jobs', { _id: mongojs.ObjectId( id ) }, data );
}

function removeJob( jobId ) {
  return remove( 'jobs', jobId );
}

/**
 * Get active jobs
 */
function getActiveJobs() {
  return get( 'jobs', { active: true } );
}

/**
 * Get active jobs
 */
function getProductJobs() {
  return get( 'jobs', { active: true, cohort: { $gte: 1 } } );
}

/**
 * Get jobs
 */
function getJobs() {
  return get( 'jobs', {} );
}

/**
 * Get job
 */
function getJob( id ) {
  return get( 'jobs', { _id: mongojs.ObjectId( id ) } ).then( function ( records ) {
    if ( !records.length ) {
      throw new Error( { message: 'No record found', data: id } );
    }

    return records[ 0 ];
  } );
}

/**
 * Get activities
 */
function getActivities( criteria ) {
  return get( 'activities', criteria );
}

/**
 * Save activity to db
 */
function saveActivity( activity ) {
  return save( 'activities', activity ).tap( function ( savedActivity ) {
    logger.debug( 'db.saveActivity', savedActivity.id );
  } );
}

/**
 * Update activities in db
 */
function updateActivities( activities ) {
  return Promise.map( activities, updateActivity ).tap( function ( updatedActivities ) {
    logger.debug( 'db.updateActivities', 'Updated ' + updatedActivities.length + ' activities' );
  } );
}

function updateActivity( activity ) {
  return update( 'activities', { _id: activity._id }, activity );
}

/**
 * Get helper
 */
function get( collection, criteria ) {
  return db.collection( collection ).findAsync( criteria || {} );
}

/**
 * Save helper
 */
function save( collection, doc ) {
  // Save all docs to db
  return db.collection( collection ).insertAsync( doc );
}

/**
 * Update helper
 */
function update( collection, query, data ) {
  return db.collection( collection ).findAndModifyAsync( {
    query: query,
    update: { $set: data },
    new: true
  } );
}

function remove( collection, id ) {
  return db.collection( collection ).removeAsync( { _id: id } );
}

/**
 * Drop collection
 */
function drop( collection ) {
  return db.collection( collection ).removeAsync( {} );
}

module.exports = {
  init: init,
  getActivities: getActivities,
  saveActivity: saveActivity,
  updateActivities: updateActivities,
  updateActivity: updateActivity,
  saveAthlete: saveAthlete,
  updateAthlete: updateAthlete,
  saveJob: saveJob,
  updateJob: updateJob,
  getJobs: getJobs,
  getJob: getJob,
  getActiveJobs: getActiveJobs,
  getProductJobs: getProductJobs,
  removeJob: removeJob,
  drop: drop,
  save: save
};

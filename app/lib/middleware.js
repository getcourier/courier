// External dependencies
var basicAuth = require( 'basic-auth' );

// Dependencies
var environment = require( './environment' );

/**
 * Simple basic auth middleware for use with Express 4.x.
 *
 * @example
 * app.use('/api-requiring-auth', utils.basicAuth('username', 'password'));
 *
 * @param   {string}   username Expected username
 * @param   {string}   password Expected password
 * @returns {function} Express 4 middleware requiring the given credentials
 */
var auth = function ( req, res, next ) {
  var user = basicAuth( req );

  if ( !user || user.name !== environment.auth.username || user.pass !== environment.auth.password ) {
    res.set( 'WWW-Authenticate', 'Basic realm=Authorization Required' );
    return res.sendStatus( 401 );
  }

  return next();
};

module.exports = {
  auth: auth
};